import Info from "./components/Info"

function App() {
  var a = "Minh";
  return (
    <div>
      <Info
        firstName={a}
        lastName="Le Hoang"
        favNumber={23}
        career={<>Web developer</>}
      />
    </div>
  );
}

export default App;
