const { Component } = require("react");

class Info extends Component {
    render() {
        let { lastName, firstName, favNumber } = this.props;
        console.log(this.props);
        return (
            <div>
                My name is&nbsp;
                {firstName}&nbsp;
                {lastName} and my
                favourite number is&nbsp; 
                {favNumber}
            </div>
        )
    }
}

export default Info